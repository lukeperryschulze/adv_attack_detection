import pandas as pd
import os

import torch

os.environ['TRANSFORMERS_CACHE'] = 'models'
from transformers import set_seed
from transformers import pipeline
from transformers import AutoTokenizer
import numpy as np
import matplotlib.pyplot as plt
# Following packages are needed to remove stop words
from nltk.corpus import stopwords
from nltk.corpus import wordnet
import nltk
# Following packages are needed for the POS_filter
from nltk.tokenize import word_tokenize
nltk.download('punkt', download_dir='../data/nltk_data')
nltk.download('averaged_perceptron_tagger', download_dir='../data/nltk_data')
nltk.download('universal_tagset', download_dir='../data/nltk_data')
nltk.download('stopwords', download_dir='../data/nltk_data')
nltk.data.path.append('data/nltk_data')
# Needed for USE
import tensorflow.compat.v1 as tf
tf.disable_v2_behavior()
import tensorflow_hub as hub
# for parallelization
from joblib import Parallel, delayed
import pickle
# Vars because my Pc is slow
import random

torch.manual_seed(9001)
random.seed(9001)
np.random.seed(9001)
torch.manual_seed(9001)
torch.cuda.manual_seed_all(9001)
set_seed(9001)


data = pd.read_csv(r'data\IMDB_Dataset.csv'.replace('\\', '/'))

# Filter the dataset to include only reviews with less than 250 words
# average number of characters of word: 4.7
# 4.7 * 250 = 1175
filtered_data = data[data['review'].str.len() < 1175]
data = filtered_data
text_data = list(data['review'].values)

model_name = 'distilbert-base-uncased-finetuned-sst-2-english'

# Initialize according model
tokenizer = AutoTokenizer.from_pretrained(model_name)
# We added tok_k = None, in order to ouput all scores
model = pipeline('sentiment-analysis', model=model_name, tokenizer=tokenizer, truncation=True)



texts = data['review'].tolist()
labels = data['sentiment'].tolist()

# Calculate accuracy
correct_predictions = 0
total_predictions = len(texts)

for text, label in zip(texts, labels):
    result = model(text)[0]
    predicted_label = result['label'].lower()
    print(predicted_label)
    print(label)

    if predicted_label == label:
        correct_predictions += 1

accuracy = correct_predictions / total_predictions
print(f"Accuracy: {accuracy * 100:.2f}%")


import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

data = pd.read_csv(r'C:\Users\lukes\PycharmProjects\NLP_LAB\data\IMDB Dataset.csv')
text_data = data['review'].values



tokenized_text_data_len = [len(text.split()) for text in text_data]

# Calculate average, minimum, and maximum values
avg_value = np.mean(tokenized_text_data_len)
min_value = np.min(tokenized_text_data_len)
max_value = np.max(tokenized_text_data_len)

# Create the plot
plt.plot(range(0, 50000), tokenized_text_data_len, linewidth=0.05)
plt.axhline(avg_value, color='red', linestyle='--', label='Average')
plt.axhline(min_value, color='green', linestyle='--', label='Minimum')
plt.axhline(max_value, color='blue', linestyle='--', label='Maximum')

plt.xlabel('Text Number')
plt.ylabel('Word Length')
plt.title('Word Length vs Text Number')
plt.grid(True)


# Calculate percentiles
percentiles = np.percentile(tokenized_text_data_len, [25, 50, 75])

# Plot percentile lines
for i, percentile in enumerate(percentiles):
    plt.axhline(percentile, color='gray', linestyle='--')
    plt.text(50000, percentile, f'{[25, 50, 75][i]}th percentile', ha='right', va='center', fontsize=8)

plt.legend()
# Save the plot
plt.savefig('plots/word_length_plot.png')
# Display the plot
plt.show()
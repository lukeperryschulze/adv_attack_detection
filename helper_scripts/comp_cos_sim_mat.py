import numpy as np
import sys

embedding_path = '../data/counter-fitted-vectors.txt'


embeddings = []
with open(embedding_path, 'r', encoding = 'utf8') as ifile:
    for line in ifile:
        embedding = [float(num) for num in line.strip().split()[1:]]
        embeddings.append(embedding)
embeddings = np.array(embeddings)

embeddings = embeddings[:30000]

print(embeddings.T.shape)
# calculate the norm, in order to calculate cosine value
norm = np.linalg.norm(embeddings, axis=1, keepdims=True)
embeddings = np.asarray(embeddings / norm, "float32")
product = np.dot(embeddings, embeddings.T)
np.save((r'data/cos_sim_embedding_matrix.npy'), product)


# Here we save the names of the embeddings

'''
with open(embedding_path, 'r', encoding='utf8') as ifile:
    names = [line.strip().split()[0] for line in ifile]
'''
import numpy as np
# from .utils import worker_process, worker_init, attack_process
import pickle

IMDB = False
YELP = False

if IMDB:
    with open('../data/results/textfoolerresults.pkl', 'rb') as file:
        text_fooler_results= pickle.load(file)

    with open('../data/results/textfooler_success.pkl', 'rb') as file:
        text_fooler_success = pickle.load(file)

    with open('../data/results/dwbresults.pkl', 'rb') as file:
        dwb_results= pickle.load(file)

    with open('../data/results/dwb_success.pkl', 'rb') as file:
        dwb_success = pickle.load(file)


if YELP:
    with open('../data/results/textfoolerresults_yelp.pkl', 'rb') as file:
        text_fooler_results= pickle.load(file)

    with open('../data/results/textfooler_yelp_success.pkl', 'rb') as file:
        text_fooler_success = pickle.load(file)

    with open('../data/results/dwbresults_yelp.pkl', 'rb') as file:
        dwb_results= pickle.load(file)

    with open('../data/results/dwb_success_yelp.pkl', 'rb') as file:
        dwb_success = pickle.load(file)

### load data for N evaluation
with open('../data/results/textfoolerresults.pkl', 'rb') as file:
    text_fooler_results= pickle.load(file)
    text_fooler_results = text_fooler_results[:1000]

with open('../data/results/textfoolerresults_epsilon095_dict_success.pkl', 'rb') as file:
    text_fooler_success = pickle.load(file)


x = text_fooler_results
average_attack_deviation = np.mean([attack['original_score'] - attack['new_score'] for attack in x])
variance_attack_deviation = np.var([attack['original_score'] - attack['new_score'] for attack in x])
max_attack_deviation = np.max([attack['original_score'] - attack['new_score'] for attack in x])
min_attack_deviation = np.min([attack['original_score'] - attack['new_score'] for attack in x])
success_fooler = len(text_fooler_success)/len(text_fooler_results)



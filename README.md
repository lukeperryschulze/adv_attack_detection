# Adversarial Text Generation Algorithms and Reconstructor Networks

This repository contains implementations of two state-of-the-art adversarial text generation algorithms and reconstructor networks for NLP adversarial attacks. We have implemented the TextFooler algorithm by Jin et al. and the DeepWordBug algorithm by Gao et al. In addition, we have re-implemented the reconstructor networks proposed by Ko et al., with the aim of reproducing their results in the field of NLP adversarial attacks.

## Algorithms Implemented

### 1. TextFooler by Jin et al.
- TextFooler is an adversarial text generation algorithm that aims to generate adversarial examples to deceive natural language processing (NLP) models.
- **Citation:** [TextFool: Fool your Model with Natural Adversarial Text](https://groups.csail.mit.edu/medg/ftp/psz-papers/2019%20Di%20Jin.pdf) - Jin et al., 2019.

### 2. DeepWordBug by Gao et al.
- DeepWordBug is another adversarial text generation algorithm that focuses on crafting minimal character-level perturbations to deceive NLP models.
- **Citation:** [Black-box Generation of Adversarial Text Sequences to Evade Deep Learning Classifiers](https://arxiv.org/abs/1811.00138) - Gao et al., 2018.

## Reconstructor Networks

We have re-implemented the reconstructor networks proposed by Ko et al. These networks are designed to enhance the robustness of NLP models by reconstructing the explanations from adversarial examples and non-adversarial sequences. Our goal is to reproduce the results achieved by Ko et al. in the context of NLP adversarial attacks.


The process involves three key stages:

1. **Adversarial Example Generation**: We generate a variety of adversarial examples using popular NLP adversarial attacks. These attacks target sentiment classification models with benchmark datasets.

2. **Reconstructor Network as Autoencoder**: Inspired by Ko and Lim (2021), we create Autoencoders tailored to each class label. These Autoencoders are trained to reconstruct original explanations. Our objective is to make original explanations easier to reconstruct than adversarial ones.

3. **Reconstructor as Classifier**: In the final stage, we utilize the trained Autoencoders as a classification tool. Given a suspicious input, we determine its class label and explanation. If the reconstruction error of the explanation exceeds a predefined threshold, we classify the input as adversarial (Ko and Lim, 2021).

These stages collectively form our pipeline for detecting and classifying adversarial examples in NLP tasks.
- **Citation:** [Unsupervised Detection of Adversarial Examples
with Model Explanations](https://arxiv.org/pdf/2107.10480) - Ko et al., 2021.


## Explanations Generation

The explanations for the adversarial examples have been generated using the Integrated Gradients technique with the assistance of the Transformers Interpret library.

# TODO List

- **Cleanup**: Ongoing maintenance and organization.
- **Hyperparameter Optimization**: Improve Reconstructor network's differentiation ability.
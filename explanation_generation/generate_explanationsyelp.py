import os
os.environ['TRANSFORMERS_CACHE'] = 'models'
from transformers import AutoModelForSequenceClassification, AutoTokenizer, pipeline
import pandas as pd
import pickle
from transformers_interpret import SequenceClassificationExplainer
from transformers import set_seed
import random
import torch
import numpy as np
#####
torch.manual_seed(9001)
random.seed(9001)
np.random.seed(9001)
torch.manual_seed(9001)
torch.cuda.manual_seed_all(9001)
set_seed(9001)
#####


model_name = 'Ramamurthi/distilbert-base-uncased-finetuned-yelp-reviews'
model_for_transformers_interpret = AutoModelForSequenceClassification.from_pretrained(model_name)
tokenizer = AutoTokenizer.from_pretrained(model_name)
# We added tok_k = None, in order to ouput all scores
model = pipeline('sentiment-analysis', model=model_name, tokenizer=tokenizer, truncation=True)
# With both the model and tokenizer initialized we are now able to get explanations on an example text.


cls_explainer = SequenceClassificationExplainer(
    model_for_transformers_interpret,
    tokenizer)

with open(r'../data/results/textfooler_yelp_success.pkl', 'rb') as file:
    succ_attacks = pickle.load(file)

adv_texts = [attack['adv_text'] for attack in succ_attacks]

results = model(adv_texts)

onestar_indices = [i for i, result in enumerate(results) if result['label'] == '1 star']
twostar_indices = [i for i, result in enumerate(results) if result['label'] == '2 star']
threestar_indices = [i for i, result in enumerate(results) if result['label'] == '3 star']
fourstar_indices = [i for i, result in enumerate(results) if result['label'] == '4 star']
fivestar_indices = [i for i, result in enumerate(results) if result['label'] == '5 star']


# fix max. number of explanations
max_number_of_explanations = 1000

onestar_indices = onestar_indices[:max_number_of_explanations]
twostar_indices = twostar_indices[:max_number_of_explanations]
threestar_indices = threestar_indices[:max_number_of_explanations]
fourstar_indices = fourstar_indices[:max_number_of_explanations]
fivestar_indices = fivestar_indices[:max_number_of_explanations]

print(len(onestar_indices))
print(len(twostar_indices))
print(len(threestar_indices))
print(len(fourstar_indices))
print(len(fivestar_indices))

onestar_explanations = [cls_explainer(adv_texts[i]) for i in onestar_indices]
twostar_explanations = [cls_explainer(adv_texts[i]) for i in twostar_indices]
threestar_explanations = [cls_explainer(adv_texts[i]) for i in threestar_indices]
fourstar_explanations = [cls_explainer(adv_texts[i]) for i in fourstar_indices]
fivestar_explanations = [cls_explainer(adv_texts[i]) for i in fivestar_indices]


'''
onestar_explanations_dicts = []
twostar_explanations_dicts = []
threestar_explanations_dicts = []
fourstar_explanations_dicts = []
fivestar_explanations_dicts = []


for i, index in enumerate(onestar_indices):
    succ_attacks[index]['explanation'] = onestar_explanations[i]
    onestar_explanations_dicts.append(adv_texts[index])

for i, index in enumerate(twostar_indices):
    adv_texts[index]['explanation'] = twostar_explanations[i]
    twostar_explanations_dicts.append(adv_texts[index])

for i, index in enumerate(threestar_indices):
    adv_texts[index]['explanation'] = threestar_explanations[i]
    threestar_explanations_dicts.append(adv_texts[index])

for i, index in enumerate(fourstar_indices):
    adv_texts[index]['explanation'] = fourstar_explanations[i]
    fourstar_explanations_dicts.append(adv_texts[index])

for i, index in enumerate(fivestar_indices):
    adv_texts[index]['explanation'] = fivestar_explanations[i]
    fivestar_explanations_dicts.append(adv_texts[index])


explanations = {
    '1_star_explanations': onestar_explanations_dicts,
    '2_star_explanations': twostar_explanations_dicts,
    '3_star_explanations': threestar_explanations_dicts,
    '4_star_explanations': fourstar_explanations_dicts,
    '5_star_explanations': fivestar_explanations_dicts,
}
'''

explanations = {
    '1_star_explanations': onestar_explanations,
    '2_star_explanations': twostar_explanations,
    '3_star_explanations': threestar_explanations,
    '4_star_explanations': fourstar_explanations,
    '5_star_explanations': fivestar_explanations,
}

with open('data/explanations/textfooler_yelp_5starexplanations.pkl', 'wb') as file:
    pickle.dump(explanations, file)

import os
os.environ['TRANSFORMERS_CACHE'] = 'models'
from transformers import AutoModelForSequenceClassification, AutoTokenizer, pipeline
import pandas as pd
import pickle
from transformers_interpret import SequenceClassificationExplainer
from transformers import set_seed
import random
import torch
import numpy as np
#####
torch.manual_seed(9001)
random.seed(9001)
np.random.seed(9001)
torch.manual_seed(9001)
torch.cuda.manual_seed_all(9001)
set_seed(9001)
#####


model_name = "distilbert-base-uncased-finetuned-sst-2-english"
model_for_transformers_interpret = AutoModelForSequenceClassification.from_pretrained(model_name)
tokenizer = AutoTokenizer.from_pretrained(model_name)
# We added tok_k = None, in order to ouput all scores
model = pipeline('sentiment-analysis', model=model_name, tokenizer=tokenizer, truncation=True)
# With both the model and tokenizer initialized we are now able to get explanations on an example text.


cls_explainer = SequenceClassificationExplainer(
    model_for_transformers_interpret,
    tokenizer)

with open(r'../data/results/textfooler_success.pkl', 'rb') as file:
    succ_attacks = pickle.load(file)

adv_texts = [attack['adv_text'] for attack in succ_attacks]

results = model(adv_texts)

positive_indices = [i for i, result in enumerate(results) if result['label'] == 'POSITIVE']
negative_indices = [i for i, result in enumerate(results) if result['label'] == 'NEGATIVE']

# fix max. number of explanations
max_number_of_explanations = 1000
positive_indices = positive_indices[:max_number_of_explanations]
negative_indices = negative_indices[:max_number_of_explanations]


pos_explanations = [cls_explainer(adv_texts[i]) for i in positive_indices]
neg_explanations = [cls_explainer(adv_texts[i]) for i in negative_indices]

pos_explanations_dicts = []
neg_explanations_dicts = []

for i, index in enumerate(positive_indices):
    adv_texts[index]['explanation'] = pos_explanations[i]
    pos_explanations_dicts.append(adv_texts[index])

for i, index in enumerate(negative_indices):
    adv_texts[index]['explanation'] = neg_explanations[i]
    neg_explanations_dicts.append(adv_texts[index])

explanations = {
    'positive_explanations': pos_explanations_dicts,
    'negative_explanations': neg_explanations_dicts
}


with open('data/explanations/textfooler_explanations_dicts.pkl', 'wb') as file:
    pickle.dump(explanations, file)





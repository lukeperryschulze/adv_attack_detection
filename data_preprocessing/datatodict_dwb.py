import pickle
import pandas as pd
import numpy as np

with open(r'../data/results/deep_word_bug_scores2.pkl', 'rb') as file:
    score_data_2 = pickle.load(file)

with open(r'../data/results/dwb_scores.pkl', 'rb') as file:
    score_data = pickle.load(file)
# Load IMDB data
# data = pd.read_csv(r'data\IMDB_Dataset.csv'.replace('\\', '/'))
data = pd.read_csv(r'../data/IMDB_Dataset.csv')
data = data[data['review'].str.len() < 1175]
text_data = data['review'].values

def dwb_transform(data):
    new_scores = score_data[0][1]
    new_scores_2 = score_data_2[0][1]
    indices = [index for index, value in enumerate(new_scores) if value < 0.5]
    indices_2 = [index for index, value in enumerate(new_scores_2) if value < 0.5]

    #Init dict for successful attacks
    succ_attacks = []
    for i, index in enumerate(indices):
        succ_attacks.append({
            'original_score': score_data[0][0][index],
            'new_score': score_data[0][1][index],
            'text': text_data[index],
            'adv_text': score_data[1][index]
        })

    for i, index in enumerate(indices_2):
        succ_attacks.append({
            'original_score': score_data_2[0][0][index],
            'new_score': score_data_2[0][1][index],
            'text': text_data[index + 10000],
            'adv_text': score_data_2[1][index]
        })

    attacks = []


    for i in range(len(new_scores)):
        attacks.append({
            'original_score': score_data[0][0][i],
            'new_score': score_data[0][1][i],
            'text': text_data[i],
            'adv_text': score_data[1][i]
        })

    for i in range(len(new_scores_2)):
        attacks.append({
            'original_score': score_data[0][0][i],
            'new_score': score_data[0][1][i],
            'text': text_data[i],
            'adv_text': score_data[1][i]
        })



    return attacks, succ_attacks

x, y = dwb_transform(data)

average_attack_deviation = np.mean([attack['original_score'] - attack['new_score'] for attack in x])


#with open('data/results/dwbresults.pkl', 'wb') as file:
#    pickle.dump(x, file)

#with open('data/results/dwb_success.pkl', 'wb') as file:
#    pickle.dump(y, file)

# Success: 0.5561
# average deviation: 0.529
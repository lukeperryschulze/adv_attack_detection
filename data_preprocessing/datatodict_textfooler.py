import pickle
import pandas as pd
import numpy as np

# data = pd.read_csv(r'data\IMDB_Dataset.csv'.replace('\\', '/'))
data = pd.read_csv(r'../data/IMDB_Dataset.csv')
data = data[data['review'].str.len() < 1175]
text_data = data['review'].values


with open('../data/results/text_foolerscores_1.pkl', 'rb') as file:
    scores = pickle.load(file)[:1000]


def scores_to_dict(scores):
    attacks = []
    for i, score in enumerate(scores):
        if len(score) == 3:
            attacks.append({
                'original_score': score[2],
                'new_score': score[1],
                'text': text_data[i],
                'adv_text': score[0]
            })
        if len(score) == 4:
            attacks.append({
                'original_score': score[3],
                'new_score': score[2],
                'text': text_data[i],
                'adv_text': score[0]
            })

    succ_attacks = [attack for attack in attacks if attack['new_score'] < 0.5]

    return attacks, succ_attacks


tf_attacks, tf_succ_attacks = scores_to_dict(scores)


average_attack_deviation = np.mean([attack['original_score'] - attack['new_score'] for attack in tf_attacks])

'''
with open('data/results/textfoolerresults_epsilon095_dict.pkl', 'wb') as file:
    pickle.dump(tf_attacks, file)

# Success: 0.64
# average deviation = 0.42
with open('data/results/textfoolerresults_epsilon095_dict_success.pkl', 'wb') as file:
    pickle.dump(tf_succ_attacks, file)
'''
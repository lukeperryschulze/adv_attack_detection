import pickle
import pandas as pd
import numpy as np
import torch
import random
import os
os.environ['TRANSFORMERS_CACHE'] = 'models'
from transformers import set_seed

#####
torch.manual_seed(9001)
random.seed(9001)
np.random.seed(9001)
torch.manual_seed(9001)
torch.cuda.manual_seed_all(9001)
set_seed(9001)
from transformers import pipeline
from transformers import AutoTokenizer

with open(r'../data/results/deep_word_bug_scoresyelp.pkl', 'rb') as file:
    score_data = pickle.load(file)

with open(r'../data/results/deep_word_bug_scoresyelp2.pkl', 'rb') as file:
    score_data_2 = pickle.load(file)
# Load yelp data
# data = pd.read_csv(r'data\IMDB_Dataset.csv'.replace('\\', '/'))
data = pd.read_parquet(r'data/yelp_review_dataset/yelp_review_full-test.parquet'.replace('\\', '/'))
data = data[data['text'].str.len() < 1000]
text_data = list(data['text'].values)
model_name = 'Ramamurthi/distilbert-base-uncased-finetuned-yelp-reviews'
tokenizer = AutoTokenizer.from_pretrained(model_name)
# We added tok_k = None, in order to ouput all scores
model = pipeline('sentiment-analysis', model=model_name, top_k = None, tokenizer=tokenizer, truncation=True)

def dwb_transform(data):
    new_scores = score_data[0][1]
    new_scores_2 = score_data_2[0][1]
    indices = [index for index, value in enumerate(new_scores) if value < 0.5]
    indices_2 = [index for index, value in enumerate(new_scores_2) if value < 0.5]

    #Init dict for successful attacks
    succ_attacks = []
    for i, index in enumerate(indices):
        succ_attacks.append({
            'original_score': score_data[0][0][index],
            'new_score': score_data[0][1][index],
            'text': text_data[index],
            'adv_text': score_data[1][index]
        })

    for i, index in enumerate(indices_2):
        succ_attacks.append({
            'original_score': score_data_2[0][0][index],
            'new_score': score_data_2[0][1][index],
            'text': text_data[index + 10000],
            'adv_text': score_data_2[1][index]
        })

    attacks = []


    for i in range(len(new_scores)):
        attacks.append({
            'original_score': score_data[0][0][i],
            'new_score': score_data[0][1][i],
            'text': text_data[i],
            'adv_text': score_data[1][i]
        })

    for i in range(len(new_scores_2)):
        attacks.append({
            'original_score': score_data[0][0][i],
            'new_score': score_data[0][1][i],
            'text': text_data[i],
            'adv_text': score_data[1][i]
        })



    return attacks, succ_attacks

attack_dict, _ = dwb_transform(data)

average_attack_deviation = np.mean([attack['original_score'] - attack['new_score'] for attack in attack_dict])

# Success: 0.8
# average deviation: 0.3699



with open('../data/results/dwbresults_yelp.pkl', 'wb') as file:
    pickle.dump(attack_dict, file)



results = model(text_data[:20000])


adv_attacks = [attack['adv_text'] for attack in attack_dict]

adv_attack_results = model(adv_attacks)

succ_attacks = [attack_dict[i] for i in range(len(attack_dict)) if results[i][0]['label'] != adv_attack_results[i][0]['label']]

with open('../data/results/dwb_success_yelp.pkl', 'wb') as file:
    pickle.dump(succ_attacks, file)
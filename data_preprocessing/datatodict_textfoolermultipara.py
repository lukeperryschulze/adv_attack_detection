import pickle
import pandas as pd
import numpy as np
import pandas as pd
import os

import torch

os.environ['TRANSFORMERS_CACHE'] = 'models'
from transformers import set_seed
import random
#####
torch.manual_seed(9001)
random.seed(9001)
np.random.seed(9001)
torch.manual_seed(9001)
torch.cuda.manual_seed_all(9001)
set_seed(9001)
from transformers import pipeline
from transformers import AutoTokenizer

#data = pd.read_csv(r'data\IMDB_Dataset.csv'.replace('\\', '/'))
#data = pd.read_csv(r'data\IMDB_Dataset.csv')
#data = data[data['review'].str.len() < 1175]
#text_data = data['review'].values

data = pd.read_parquet(r'data/yelp_review_dataset/yelp_review_full-test.parquet'.replace('\\', '/'))
data = data[data['text'].str.len() < 1000]
text_data = list(data['text'].values)

with open('../data/results/text_foolerscores_yelp1.pkl', 'rb') as file:
    scores_1 = pickle.load(file)

with open('../data/results/text_foolerscores_yelp2.pkl', 'rb') as file:
    scores_2 =pickle.load(file)

with open('../data/results/text_foolerscores_yelp3.pkl', 'rb') as file:
    scores_3 =pickle.load(file)

with open('../data/results/text_foolerscores_yelp4.pkl', 'rb') as file:
    scores_4 =pickle.load(file)

scores = scores_1 + scores_2 + scores_3 + scores_4

def scores_to_dict(scores):
    attacks = []
    succ_attacks = []
    for i, score in enumerate(scores):
        if len(score) == 3:
            attacks.append({
                'original_score': score[2],
                'new_score': score[1],
                'text': text_data[i],
                'adv_text': score[0]
            })
        if len(score) == 4:
            attacks.append({
                'original_score': score[3],
                'new_score': score[2],
                'text': text_data[i],
                'adv_text': score[1]
            })
            succ_attacks.append({
                'original_score': score[3],
                'new_score': score[2],
                'text': text_data[i],
                'adv_text': score[1]
            })

    return attacks, succ_attacks


tf_attacks, tf_succ_attacks = scores_to_dict(scores)



average_attack_deviation = np.mean([attack['original_score'] - attack['new_score'] for attack in tf_attacks])

model_name = 'Ramamurthi/distilbert-base-uncased-finetuned-yelp-reviews'


##########################
'''FILTER OUT SUCC ATTACKS'''
# Initialize according model
tokenizer = AutoTokenizer.from_pretrained(model_name)
# We added tok_k = None, in order to ouput all scores
model = pipeline('sentiment-analysis', model=model_name, top_k = None, tokenizer=tokenizer, truncation=True)


#results = model(text_data[:10])

#adv_attacks = [attack['adv_text'] for attack in tf_attacks]

#adv_attack_results = model(adv_attacks[:10])

#tf_succ_attacks = [tf_attacks[i] for i in range(len(tf_attacks)) if results[i]['label'] != adv_attack_results[i]['label']]
#################

with open('../data/results/textfoolerresults_yelp.pkl', 'wb') as file:
    pickle.dump(tf_attacks, file)

# Success: 0.752875
# average deviation = 0.206
# Why? Don't need as much deviation to shift the label
with open('../data/results/textfooler_yelp_success.pkl', 'wb') as file:
    pickle.dump(tf_succ_attacks, file)
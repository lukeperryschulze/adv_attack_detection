import os
os.environ['TRANSFORMERS_CACHE'] = 'models'
from transformers import pipeline
from transformers import AutoTokenizer, AutoModelForSequenceClassification
import torch
import torch.nn.functional as F
import numpy as np
import pandas as pd
import random
import string
import pickle
import matplotlib.pyplot as plt
import seaborn as sns


IMDB_eval = False
Twitter_eval = False
Yelp_eval = True

# Read in IMDB file
if IMDB_eval:
    data = pd.read_csv(r'data\IMDB_Dataset.csv'.replace('\\', '/'))

    # Filter the dataset to include only reviews with less than 250 words
    # average number of characters of word: 4.7
    # 4.7 * 250 = 1175
    filtered_data = data[data['review'].str.len() < 1175]
    data = filtered_data
    data = data[10000:20000]
    text_data = list(data['review'].values)

    model_name = 'distilbert-base-uncased-finetuned-sst-2-english'

# Read in Yelp file:
if Yelp_eval:
    data = pd.read_parquet(r'../data/yelp_review_dataset/yelp_review_full-test.parquet')
    data = data[data['text'].str.len() < 1000]
    text_data = list(data['text'].values)
    model_name = 'Ramamurthi/distilbert-base-uncased-finetuned-yelp-reviews'


tokenizer = AutoTokenizer.from_pretrained(model_name)
# We added top_k = None, in order to ouput all scores
model = pipeline('sentiment-analysis', model=model_name, tokenizer=tokenizer, top_k=None, truncation=True)




#results = model(text_data[0:50])



''' We do not want to work with normal tokenizers when changing the meaning of a word, 
    due to the reversibility of those being nontrivial '''

def replace_word_with_unk(text, i):
    words = text.split()
    if i < 0 or i >= len(words):
        return text
    words[i] = '[UNK]'
    return ' '.join(words)


# Scoring function (needed to determine the importance of the tokens)
def replace_score(input_sequence, Jin_eval = False):
    # Calculate the result of the unaltered sequence
    input_result = model(input_sequence)
    classification_of_input_sequence = input_result[0][0]['label']
    score_of_input_sequence = input_result[0][0]['score']
    print('Score of the input sequence' + str(input_result[0][0]['score']))

    # Setup array, which saves the score difference between unaltered and altered classification
    score_difference = np.zeros(len(input_sequence.split()))
    score_difference += score_of_input_sequence
    for i in range(len(input_sequence.split())):
        # Calculate modified input by replacing the i'th word with [UNK]
        modified_input = replace_word_with_unk(input_sequence, i)
        # print('modified input: ' + modified_input)
        # Calculate the results of the classification of the modified input
        result = model(modified_input)

        '''In this loop we loop over the scores of the results
            We do this in order to find the result, which matches the label of our input_sequence'''
        for j in range(len(result[0])):
            if (result[0][j]['label'] == classification_of_input_sequence):
                print('Modified score for the given modified result' + str(result[0][j]['score']))
                score_difference[i] = score_difference[i] - result[0][j]['score']
                break
        if Jin_eval:
            if result[0][0]['label'] != classification_of_input_sequence:
                for j in range(len(input_result[0])):
                    if input_result[0][j]['label'] == result[0][0]['label']:
                        score_difference[i] = score_difference[i] - result[0][j]['score']
                        break
                score_difference[i] = score_difference[i] + result[0][0]['score'] - input_result[0][j]['score']

    # print(score_difference)
    return score_difference

def apply_random_transformation(word, cost):
    transformation = random.choice(['delete', 'substitute', 'swap', 'insert'])
    if len(word) == 1:
        transformation = 'insert'
    if transformation == 'delete':
        # Delete a random letter from the word
        index = random.randint(0, len(word) - 1)
        new_word = word[:index] + word[index + 1:]
        cost += 1
    elif transformation == 'substitute':
        # Substitute a random letter in the word with a random letter
        index = random.randint(0, len(word) - 1)
        new_letter = random.choice(string.ascii_lowercase)
        new_word = word[:index] + new_letter + word[index + 1:]
        cost += 1
    elif transformation == 'swap':
        # Swap two adjacent letters in the word
        index = random.randint(0, len(word) - 2)
        new_word = word[:index] + word[index + 1] + word[index] + word[index + 2:]
        cost += 2
    elif transformation == 'insert':
        # Insert a random letter into the word at a random position
        index = random.randint(0, len(word))
        new_letter = random.choice(string.ascii_lowercase)
        new_word = word[:index] + new_letter + word[index:]
        cost += 1
    else:
        # No transformation, return the original word
        new_word = word

    return new_word, cost


# DeepWordBug algorithm
def attack(input_sequence, tokenizer = AutoTokenizer.from_pretrained(model_name), epsilon = 30):

    score_differences = replace_score(input_sequence)
    args = score_differences.argsort()[::-1]

    print(args)
    words = input_sequence.split()
    cost = 0
    counter = 0
    while (cost < epsilon and counter < len(args)):
        words[args[counter]], cost = apply_random_transformation(words[args[counter]], cost)
        counter += 1

    return ' '.join(words)

def dump_results(results, pickle_file_path='data/results/deep_word_bug_scoresyelp.pkl'):
    # Dump the list into the pickle file
    with open(pickle_file_path, "wb") as f:
        pickle.dump(results, f)

# Input: output of model (normal and adversatrial)
# Output: scores, attack scores
def map_score_values(results, attack_results):
    # Save all scores into a list, w.r.t ordering
    results_to_list = [results[i][0]['score'] for i in range(len(results))]
    labels_to_list = [results[i][0]['label'] for i in range(len(results))]

    # Match attack_results to the first classification

    attack_results_label = [attack_results[i][0]['label'] for i in range(len(results))]

    attack_results_to_list = []

    for i in range(len(attack_results)):
        # If score changed add second score
        if attack_results_label[i] != labels_to_list[i]:
            # Go through the entire list of scores and find the matching one to the original label
            for j in range(len(attack_results[i])):
                if attack_results[i][j]['label'] == labels_to_list[i]:
                    attack_results_to_list.append(attack_results[i][1]['score'])
        # If score does not change add first score
        else:
            attack_results_to_list.append(attack_results[i][0]['score'])

    return results_to_list, attack_results_to_list

# Input: scores, attacked_scores
# Output: bar_plot_score comparison
def bar_plot(scores, attacked_scores):
    # Sample score values
    score1 = scores
    score2 = attacked_scores

    # Create an array of indices for the bars
    indices = np.arange(len(score1))

    # Set the width of each bar
    bar_width = 0.35

    # Create the bar plot
    fig, ax = plt.subplots()
    rects1 = ax.bar(indices, score1, bar_width, label='Scores')
    rects2 = ax.bar(indices + bar_width, score2, bar_width, label='Scores after Attack')

    # Add labels and title
    ax.set_xlabel('Input Data')
    ax.set_ylabel('Scores')
    ax.set_title('Comparison of Score Values')
    ax.set_xticks(indices + bar_width / 2)
    ax.set_xticklabels(indices + 1)

    # Add a legend
    ax.legend()

    # Show the plot
    # plt.show()

    plt.savefig('plots/deepwordbug.png')

    return None


def main():
    # Evaluation
    pass





if __name__ == '__main__':
    random.seed(9001)
    torch.manual_seed(9001)

    # Evaluation
    num_evals = 10000
    attack_list = [attack(text_data[i]) for i in range(num_evals)]

    # calculate by results by feeding to the model
    results = model(text_data[:num_evals])
    attack_results = model(attack_list)


    original_scores, altered_scores = map_score_values(results, attack_results)
    # dump results
    dump_results([(original_scores, altered_scores), attack_list])
    # map results scores to corresponding attack_results

    # bar_plot(results, attack_results)








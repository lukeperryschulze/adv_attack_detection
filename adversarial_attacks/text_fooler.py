import pandas as pd
import os

import torch

os.environ['TRANSFORMERS_CACHE'] = 'models'
from transformers import set_seed
from transformers import pipeline
from transformers import AutoTokenizer
import numpy as np
import matplotlib.pyplot as plt
# Following packages are needed to remove stop words
from nltk.corpus import stopwords
from nltk.corpus import wordnet
import nltk
# Following packages are needed for the POS_filter
from nltk.tokenize import word_tokenize
nltk.download('punkt', download_dir='../data/nltk_data')
nltk.download('averaged_perceptron_tagger', download_dir='../data/nltk_data')
nltk.download('universal_tagset', download_dir='../data/nltk_data')
nltk.download('stopwords', download_dir='../data/nltk_data')
nltk.data.path.append('data/nltk_data')
# Needed for USE
import tensorflow.compat.v1 as tf
tf.disable_v2_behavior()
import tensorflow_hub as hub
# for parallelization
from joblib import Parallel, delayed
import pickle
# Vars because my Pc is slow
import random
#####

limit = 65713
#limit = 30000

#####
torch.manual_seed(9001)
random.seed(9001)
np.random.seed(9001)
torch.manual_seed(9001)
torch.cuda.manual_seed_all(9001)
set_seed(9001)

IMDB_eval = True
Twitter_eval = False
Yelp_eval = False

# Read in IMDB file
if IMDB_eval:
    data = pd.read_csv(r'data\IMDB_Dataset.csv'.replace('\\', '/'))

    # Filter the dataset to include only reviews with less than 250 words
    # average number of characters of word: 4.7
    # 4.7 * 250 = 1175
    filtered_data = data[data['review'].str.len() < 1175]
    data = filtered_data
    text_data = list(data['review'].values)

    model_name = 'distilbert-base-uncased-finetuned-sst-2-english'

# Read in Yelp file:
if Yelp_eval:
    data = pd.read_parquet(r'data/yelp_review_dataset/yelp_review_full-test.parquet'.replace('\\', '/'))
    data = data[data['text'].str.len() < 1000]
    text_data = list(data['text'].values)
    model_name = 'Ramamurthi/distilbert-base-uncased-finetuned-yelp-reviews'

# Step 1 : Word Importance Ranking
'We do this by masking one token of the input at a time and compare results'


'''Download dependencies'''
# load counterfit words for word similarity

embedding_path = '../data/counter-fitted-vectors.txt'

# Here we save the names of the embeddings
with open(embedding_path, 'r', encoding='utf8') as ifile:
    index_word_list = [line.strip().split()[0] for line in ifile]

# Define a global word_index dictionary in order to look up the index of the words later on
word_index_dict = {w: i for i, w in enumerate(index_word_list)}

# Input: word
# Output: index for counterfit words, None if no idx exists
def get_word_index(word):
    return word_index_dict.get(word)

# load the cosine similarity matrix
cosine_sim_matrix = np.load('../data/cos_sim_embedding_matrix.npy')


# load USE encoder into main memory
def load_USE_encoder(module):
    with tf.Graph().as_default():
        sentences = tf.placeholder(tf.string)
        embed = hub.Module(module)
        embeddings = embed(sentences)
        session = tf.train.MonitoredSession()
    return lambda x: session.run(embeddings, {sentences: x})

encoder = load_USE_encoder('./USE')

# Initialize according model
tokenizer = AutoTokenizer.from_pretrained(model_name)
# We added tok_k = None, in order to ouput all scores
model = pipeline('sentiment-analysis', model=model_name, tokenizer=tokenizer, top_k=None, truncation=True)

# results = model(text_data[0:50])

'''Second part: Main Code'''
def remove_stop_words():
    example_sent = 'This is a sample sentence, showing off the stop words filtration'
    stop_words = set(stopwords.words('english'))


def replace_word_with_unk(text, i):
    words = text.split()
    if i < 0 or i >= len(words):
        return text
    words[i] = '[UNK]'
    return ' '.join(words)


# Scoring function (needed to determine the importance of the tokens)
# Input: text (string)
# Output: score difference
def replace_score(input_sequence, Jin_eval = True):
    # Calculate the result of the unaltered sequence
    input_result = model(input_sequence)
    classification_of_input_sequence = input_result[0][0]['label']
    score_of_input_sequence = input_result[0][0]['score']
    print('Score of the input sequence' + str(input_result[0][0]['score']))

    # Setup array, which saves the score difference between unaltered and altered classification
    score_difference = np.zeros(len(input_sequence.split()))
    score_difference += score_of_input_sequence
    for i in range(len(input_sequence.split())):
        # Calculate modified input by replacing the i'th word with [UNK]
        modified_input = replace_word_with_unk(input_sequence, i)
        # print('modified input: ' + modified_input)
        # Calculate the results of the classification of the modified input
        result = model(modified_input)

        '''In this loop we loop over the scores of the results
            We do this in order to find the result, which matches the label of our input_sequence'''
        for j in range(len(result[0])):
            if (result[0][j]['label'] == classification_of_input_sequence):
                print('Modified score for the given modified result' + str(result[0][j]['score']))
                score_difference[i] = score_difference[i] - result[0][j]['score']
                break
        if Jin_eval:
            if result[0][0]['label'] != classification_of_input_sequence:
                for j in range(len(input_result[0])):
                    if input_result[0][j]['label'] == result[0][0]['label']:
                        score_difference[i] = score_difference[i] - result[0][j]['score']
                        break
                score_difference[i] = score_difference[i] + result[0][0]['score'] - input_result[0][j]['score']

    # print(score_difference)
    return score_difference


# Input: sentence as a string
# Output: List of (word token, tag)tuples (i.e. [(token_1, pos_tag_1), ..., (token_n, pos_tag_n))]
def pos_filter(text):
    tokens = word_tokenize(text)  # Tokenize the input text
    pos_tags = nltk.pos_tag(tokens)  # Perform POS tagging
    return pos_tags

# Input: 2 sentences, target tuple (target_word, position), replacement_word (i.e. synonym)
# Output: Check if the POS is the same, return the new text sequence if it is, else return None
def grammar_comparison(text, target,  replacement_word):

    tokenized_input_sequence = text.split()
    tokenized_replacement_sequence = text.split()
    tokenized_replacement_sequence[target[1]] = replacement_word
    text_replaced = ' '.join(tokenized_replacement_sequence)

    pos_tags_original = pos_filter(text)
    pos_tags_replacement = pos_filter(text_replaced)


    # If the pos tags of the replacement and original word are the same, return True
    for w1, w2 in zip(pos_tags_original, pos_tags_replacement):
        if w1[0] == target[0] and w2[0] == replacement_word and w1[1] == w2[1]:
            return text_replaced

    # Else return None
    return None




# Input: word importance tuple (i.e. [(word1, ranking1),...,[word_n, ranking_n]])
# Output: word importance tuple list without stop words
def filter_stopwords(word_importance_tuple_list):
    stop_words = set(stopwords.words('english'))
    filtered_words = [(word, ranking) for (word, ranking) in word_importance_tuple_list
                      if word.lower() not in stop_words]
    return filtered_words


# Input: input text_sequence
# Output: word_importance_tuples sorted w.r.t. vulnerability
def generate_targets(input_sequence):
    # Calculate the score_differences in order to find vulnerable tokens
    score_differences = replace_score(input_sequence)
    args = score_differences.argsort()[::-1]
    # Split words in order to map args to the corresponding tokens
    tokenized_sequence = input_sequence.split()
    word_importance_tuples = [(tokenized_sequence[arg], arg) for arg in args]

    word_importance_tuples = filter_stopwords(word_importance_tuples)

    return word_importance_tuples


# Input: word
# Output: list of synonyms
# This function is dated, please use calculate_synonyms
def calculate_synonyms_wordnet(word):
    print('This function is dated, please use calculate_synonyms')
    synonyms = []
    for synset in wordnet.synsets(word):
        for lemma in synset.lemmas():
            synonyms.append(lemma.name())

    # Wordnet sometimes returns duplicates, in this case we filter those out
    synonyms_without_duplicates = [syn for syn in synonyms if syn != word]

    print('These are the synonyms, we are working with' + str(synonyms_without_duplicates))
    return synonyms_without_duplicates

# Input: word
# Output: list of synonyms
def calculate_synonyms(word, N = 100):
    idx = get_word_index(word)

    if idx is None:
        return []

    if idx > limit:
        return []
    # Calculate the N nearest synonyms
    N_nearest_synonyms = cosine_sim_matrix[idx, :].argsort()[::-1][:N]
    synonyms = [index_word_list[i] for i in N_nearest_synonyms]
    return synonyms

# Input: input_sequence, adversarial_sequences (iterator)
# Output: [cos(input_sequence, adversarial_sequences[0], ... cos(input_sequence, adversarial sequences[n])]
def USE_cosine_similarity(input_sequence, adversarial_sequences):

    # encode adversarial_sequences and input sequence
    sentence_encodings = encoder(adversarial_sequences + [input_sequence])
    # filter out input_sequence encoding
    input_sequence_encoded = sentence_encodings[len(sentence_encodings) - 1]
    # calculate_norms

    # calculate norm products efficiently
    norms = np.linalg.norm(sentence_encodings, axis = 1)
    # calculate products for norm division
    norm_products = norms[len(norms) - 1] * norms[0: len(norms)-1]
    cosine_similarities = [np.dot(input_sequence_encoded,
                                  sentence_encodings[i])/norm_products[i] for i in range(len(sentence_encodings)-1)]

    return cosine_similarities


# Input: input_sequence, epsilon value for semantic similarity comparison
# Output: adversarial_sequence, classification score
def attack(input_sequence, epsilon=0.75, n_value=100):
    input_classification = model(input_sequence)
    classification_of_input_sequence = input_classification[0][0]['label']
    # update global_minimum classification
    global_min_classification_sequence = input_sequence
    global_min = input_classification[0][0]['score']
    print('The input sequence was classified as' + str(classification_of_input_sequence))
    # Generate targets
    targets = generate_targets(input_sequence)

    # Init
    current_target = 0
    # Go through targets until we can find a suitable adversarial attack or have used up all our targets
    while targets[current_target] != targets[len(targets) - 1]:
        # Take the most vulnerable word as target, target = ('word', position) tuple
        target = targets[current_target]
        # Calculate the 'N' nearest synonyms
        synonyms = []
        # Looping until our synonym list is non-empty
        while not synonyms:
            synonyms = calculate_synonyms(target[0], N = n_value)
            current_target += 1
            # error handling if list index out of range, i.e. return best example so far
            if current_target == len(targets) - 1:
                return global_min_classification_sequence, global_min, input_classification[0][0]['score']
            target = targets[current_target]
        # print("This is the current attack target " + str(target))

        # tokenize input sequence TODO: this needs to be improved later on to fit to the tokenizer of the respective model
        adv_sequences_with_none = [(grammar_comparison(input_sequence, target, synonym), synonym) for synonym in synonyms]
        adv_sequences = [element for element in adv_sequences_with_none if element[0] is not None]

        # Check if adv_sequences is empty, if this is the case then we need to continue with the next target_word
        if not adv_sequences:
            print('After grammar check we were not able to produce a semantically similar sentence')
            print('We now move on to the next target')

        if adv_sequences:
            # Compute cosine similarity
            # calculate 'pure_adversarial sequences' without synonyms
            pure_adv_sequences = [sequence for sequence, synonym in adv_sequences]
            cosine_similarities = USE_cosine_similarity(input_sequence, pure_adv_sequences)

            # Filter out sequences, with low semantic similarity [(sequence, chosen_synonym, cosine_sim)]
            filtered_adv_sequences = [(adv_sequences[i][0], adv_sequences[i][1],
                                       cosine_similarities[i]) for i in range(len(adv_sequences))
                                      if cosine_similarities[i] > epsilon]

            if not filtered_adv_sequences:
                print("After computing cosine similarity the adv_attack sequences are empty. Please turn the epsilon value lower")
            else:
                # calculate 'pure_adversarial sequences' without synonyms and cosine similarities
                pure_filtered_adv_sequences = [sequence for sequence, synonym, sim_score in filtered_adv_sequences]

                attack_classifications = model(pure_filtered_adv_sequences)

                # save classification_sequences with score of first classification [(sequence, score)]
                updated_score = []
                for i in range(len(attack_classifications)):
                    for j in range(len(attack_classifications[i])):
                        if attack_classifications[i][j]['label'] == classification_of_input_sequence:
                            updated_score.append(attack_classifications[i][j]['score'])


                changed_classification_adv_sequences = [(filtered_adv_sequences[i][0], updated_score[i], filtered_adv_sequences[0][2])
                                                        for i in range(len(attack_classifications))
                                                        if attack_classifications[i][0]['label'] != classification_of_input_sequence]

                # if you cannot already alter label, calculate the lowest classification and continue with attack
                if not changed_classification_adv_sequences:
                    min = 1
                    min_classification_sequence = None
                    for i in range(len(attack_classifications)):
                        if attack_classifications[i][0]['score'] < min:
                            min = attack_classifications[i][0]['score']
                            min_classification_sequence = pure_filtered_adv_sequences[i]

                    # Update global classification to return best example in case we were not able to alter the prediction
                    if min<global_min:
                        # print('This is he new global min value' + str(min))
                        global_min = min
                        global_min_classification_sequence = min_classification_sequence

                    input_sequence = min_classification_sequence
                    current_target += 1

                    # print('The given sequence was not able to alter classification, but has the lowest confidence ' + str(input_sequence))
                    # print('Confidence' + str(min))

                # if you can already alter the classification, select the candidate with the highest semantic similarity score
                else:
                    print(changed_classification_adv_sequences)
                    sem_sim_scores = [sem_sim for _,_, sem_sim in changed_classification_adv_sequences]
                    argmax_score = np.argmax(sem_sim_scores)
                    return input_sequence, changed_classification_adv_sequences[argmax_score][0], changed_classification_adv_sequences[argmax_score][1], input_classification[0][0]['score']

    print("Could not find a working adversarial attack, Returning the best adversarial example!")
    return global_min_classification_sequence, global_min, input_classification[0][0]['score']

# Input: results, path
# Output: None, just dump scores into filepath
def dump_attackscores(results, pickle_file_path ='data/results/text_foolerscores.pkl'):
    # Dump the list into the pickle file
    with open(pickle_file_path, "wb") as f:
        pickle.dump(results, f)


# Input: list of adversarial inputs [(sequence, score, original score)]
# Output: normal scores, adversarial scores
def map_score_values(adversarial_attacks):
    scores = [attack[2] for attack in adversarial_attacks]
    adversarial_scores = [attack[1] for attack in adversarial_attacks]

    return scores, adversarial_scores

# Input: scores, attacked_scores
# Output: bar_plot_score comparison
def bar_plot(scores, attacked_scores):
    # Sample score values
    score1 = scores
    score2 = attacked_scores

    # Create an array of indices for the bars
    indices = np.arange(len(score1))

    # Set the width of each bar
    bar_width = 0.35

    # Create the bar plot
    fig, ax = plt.subplots()
    rects1 = ax.bar(indices, score1, bar_width, label='Scores')
    rects2 = ax.bar(indices + bar_width, score2, bar_width, label='Scores after Attack')

    # Add labels and title
    ax.set_xlabel('Input Data')
    ax.set_ylabel('Scores')
    ax.set_title('Comparison of Score Values')
    ax.set_xticks(indices + bar_width / 2)
    ax.set_xticklabels(indices + 1)

    # Add a legend
    ax.legend()

    # Show the plot
    # plt.show()

    plt.savefig('plots/textfooler.png')

    return None


def main():
    pass
    # evaluation
    '''
    num_evals = 5
    attack_list = []
    # generate number_of attacks
    attack_list = [attack(text_data[i]) for i in range(num_evals)]
    evaluate_attackscores(attack_list)
    '''


if __name__ == '__main__':
    ###EVAL###
    num_evals = 1000
    attack_list = []
    # generate number_of attacks
    attack_list = [attack(text_data[i], epsilon=0.5) for i in range(num_evals)]
    # save attackscores in a pickle list
    dump_attackscores(attack_list, pickle_file_path ='../data/results/text_foolerscores_epsilon050.pkl')
    # get pure score values
    attack_list = [attack(text_data[i], epsilon=0.95) for i in range(num_evals)]
    # save attackscores in a pickle list
    dump_attackscores(attack_list, pickle_file_path ='../data/results/text_foolerscores_epsilon095.pkl')

    attack_list = [attack(text_data[i], n_value=50) for i in range(num_evals)]
    # save attackscores in a pickle list
    dump_attackscores(attack_list, pickle_file_path ='../data/results/text_foolerscores_N_50.pkl')

    attack_list = [attack(text_data[i], n_value=200) for i in range(num_evals)]
    # save attackscores in a pickle list
    dump_attackscores(attack_list, pickle_file_path ='../data/results/text_foolerscores_N_200.pkl')
    # x,y = map_score_values(attack_list)
    # get plot
    # bar_plot(x, y)

    # Call attacking function, in order to construct an Adversarial Attack
    # num_jobs = 8
    # results = Parallel(n_jobs=num_jobs, prefer='threads')(delayed(attack)(data) for data in text_data[:100]
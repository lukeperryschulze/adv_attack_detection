import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.utils.data import Dataset, DataLoader
import torch.optim as optim
import matplotlib.pyplot as plt


# Custom Dataset class
class ExplanationDataset(Dataset):
    def __init__(self, data, num_tensors, tensor_size):
        self.data = data
        self.num_tensors = num_tensors
        self.tensor_size = tensor_size

    def __len__(self):
        return self.num_tensors

    def __getitem__(self, idx):
        return self.data[idx]


class EncoderDecoder(nn.Module):
    def __init__(self, input_size, hidden_size, output_size):
        super(EncoderDecoder, self).__init__()

        # Encoder
        self.encoder_layer = nn.Linear(input_size, hidden_size)

        # Decoder
        self.decoder_layer = nn.Linear(hidden_size, output_size)

    def forward(self, input_seq):
        # Encoder forward pass
        encoder_output = F.relu(self.encoder_layer(input_seq))

        # Decoder forward pass
        output = self.decoder_layer(encoder_output)

        return output

    def train_network(self, dataloader, num_epochs=3, learning_rate=0.01):
        self.train()
        criterion = nn.MSELoss()
        optimizer = optim.Adam(self.parameters(), lr=learning_rate)

        loss_list = []

        for epoch in range(num_epochs):
            running_loss = 0
            for batch in dataloader:
                inputs = batch

                # Forward pass
                outputs = self.forward(inputs)

                # print(outputs)
                # Compute the loss
                loss = criterion(outputs, inputs)
                # print(loss)
                # Zero gradients, backward pass, and optimize
                optimizer.zero_grad()
                loss.backward()
                optimizer.step()

                running_loss += loss.item()

            avg_loss = running_loss / len(dataloader)
            loss_list.append(avg_loss)

            # Print average loss after each epoch
            print(f"Epoch [{epoch + 1}/{num_epochs}], Loss: {running_loss / len(dataloader)}")

        fig, ax = plt.subplots()
        ax.plot(loss_list)
        ax.set_xlabel('Epochs')
        ax.set_ylabel('Loss')
        ax.set_title('Loss Curve')
        return fig, ax

    # return the average error on a given dataset and print the reconstructed outputs
    def test_network(self, dataloader):
        self.train()
        criterion = nn.MSELoss()

        running_loss = 0
        for batch in dataloader:
            inputs = batch

            # Forward pass
            outputs = self.forward(inputs)
            print(outputs)

            # print(outputs)
            # Compute the loss
            loss = criterion(outputs, inputs)
            loss.backward()

            running_loss += loss.item()

        avg_loss = running_loss / len(dataloader)

        # Print average loss after each epoch
        print(f"Loss: {avg_loss}")

        return avg_loss


###########################################################################


if __name__ == '__main__':
    # Example usage:
    input_size = 300
    hidden_size = 24  # 8% of input size was used in the paper
    output_size = 300

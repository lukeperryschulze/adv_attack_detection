import pandas as pd
import time  ##for performance meassureing
import numpy as np
import transformers
# from .utils import worker_process, worker_init, attack_process
import tqdm
import json
from joblib import Parallel, delayed
from joblib import Memory
import torch
import reconstructor_net
import pickle
from sklearn.preprocessing import MinMaxScaler, StandardScaler



def read_explanations(file_name="data/explanations/imdb_explanations_complete.out"):
    positive_explanations = list()
    negative_explanations = list()

    with open(file_name, 'r', encoding="utf-8") as f:
        for line in f:
            explanation = json.loads(line)
            if len(explanation["input_expl"]) < max_tokens_per_review:
                if explanation["predicted_class"] == "POSITIVE":
                    positive_explanations.append(explanation)
                elif explanation["predicted_class"] == "NEGATIVE":
                    negative_explanations.append(explanation)
    return positive_explanations, negative_explanations


def read_explanations_complete(file_name="data/explanations/imdb_explanations_complete.out", test_index = 25000):
    positive_explanations = list()
    negative_explanations = list()
    explanations = []
    with open(file_name, 'r', encoding="utf-8") as f:
        for line in f:
            explanation = json.loads(line)
            if len(explanation["input_expl"]) < max_tokens_per_review:
                explanations.append(explanation)
        explanations = explanations[test_index:]
        for explanation in explanations:
            if explanation["predicted_class"] == "POSITIVE":
                positive_explanations.append(explanation)
            elif explanation["predicted_class"] == "NEGATIVE":
                negative_explanations.append(explanation)
    return explanations, positive_explanations, negative_explanations


max_tokens_per_review = 300


###Tensor format (max_tokens_per_review) (1d):
# For every token in the review the tensor contains the respective word saliency (float)
#Input: Jan explanations
def explanation_list_to_simple_tensor(explanations_list):
    explanation_tensor = torch.zeros(1)
    for word in explanations_list["input_expl"]:
        # simply create a list of the word saliencies in the order the words occur in the review
        explanation_tensor = torch.cat((explanation_tensor, torch.tensor(word[1]).unsqueeze(0)), dim=0)

    input_len = explanation_tensor.size(0)
    if max_tokens_per_review > input_len:
        padding_len = max_tokens_per_review - input_len
        padding_tensor = torch.zeros(padding_len)
        explanation_tensor = torch.cat((explanation_tensor, padding_tensor), dim=0)

    return explanation_tensor

def explanation_to_simple_tensor_luke(explanations):
    explanation_tensor = torch.zeros(1)
    for word in explanations:
        # simply create a list of the word saliencies in the order the words occur in the review
        explanation_tensor = torch.cat((explanation_tensor, torch.tensor(word[1]).unsqueeze(0)), dim=0)

    input_len = explanation_tensor.size(0)
    if input_len > max_tokens_per_review:
        explanation_tensor = explanation_tensor[:max_tokens_per_review]
    if max_tokens_per_review > input_len:
        padding_len = max_tokens_per_review - input_len
        padding_tensor = torch.zeros(padding_len)
        explanation_tensor = torch.cat((explanation_tensor, padding_tensor), dim=0)

    return explanation_tensor





model = torch.load('data/reconstructor_models/pos_model_simple_tensors_new.pts')
#model = torch.load('data/reconstructor_models/neg_model_simple_tensors.pts')
model.eval()

_, positive_explanations, negative_explanations = read_explanations_complete()
print("Number of inputs with predicted class POSITIVE: " + str(len(positive_explanations)))
print("Number of inputs with predicted class NEGATIVE: " + str(len(negative_explanations)))
# 6k enough for training given paper
positive_explanations = positive_explanations[1000:2000]
negative_explanations = negative_explanations[:1000]


#########Test positive model

explanations_tensors_list = [explanation_list_to_simple_tensor(positive_explanation) for positive_explanation in positive_explanations]
explanations_tensors_original = torch.stack(explanations_tensors_list)

with open('data/explanations/textfooler_explanations.pkl', 'rb') as file:
    dwb_explanations = pickle.load(file)

dwb_positive = dwb_explanations['positive_explanations']

explanations_tensors_list = [explanation_to_simple_tensor_luke(positive_explanation) for positive_explanation in dwb_positive]
explanations_tensors_adversarial = torch.stack(explanations_tensors_list)

# Scale Data
scaler = MinMaxScaler((0,1))
scaled_original_data_numpy = scaler.fit_transform(explanations_tensors_original.numpy())
scaled_original_data_tensor = torch.tensor(scaled_original_data_numpy, dtype=torch.float32)

scaled_adversarial_data_numpy = scaler.fit_transform(explanations_tensors_adversarial.numpy())
scaled_adversarial_data_tensor = torch.tensor(scaled_adversarial_data_numpy, dtype=torch.float32)

explanations_tensors_original = scaled_original_data_tensor
explanations_tensors_adversarial = scaled_adversarial_data_tensor

# Output: Indices of classified adversarial inputs
def classify_inputs(autoencoder_model = model, evaluation_data = explanations_tensors_original, reconstruction_error_threshold = 3.45):
    # Pass the evaluation data through the autoencoder model to get the reconstructed outputs
    reconstructed_outputs = autoencoder_model(evaluation_data)

    # Calculate the squared L2 norm difference for each input-output pair
    l2_norms = torch.linalg.vector_norm((reconstructed_outputs - evaluation_data), dim=1)

    print('Variance' + str(torch.var(l2_norms)))
    print('Mean' + str(torch.mean(l2_norms)))

    # Compare the squared L2 norms to the reconstruction error threshold and store adversarial inputs
    adversarial_inputs = []
    for i, squared_l2_norm in enumerate(l2_norms):
        if squared_l2_norm > reconstruction_error_threshold:
            adversarial_inputs.append(i)

    return adversarial_inputs

adv_inputs_original = classify_inputs(evaluation_data=explanations_tensors_original)

print('Accuracy on original' + str(1 - len(adv_inputs_original)/1000))
adv_inputs_adversarial = classify_inputs(evaluation_data=explanations_tensors_adversarial)
print('Accuracy on adversarial' + str(len(adv_inputs_adversarial)/1000))






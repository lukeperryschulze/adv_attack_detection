import pandas as pd
import time  ##for performance meassureing
import numpy as np
import transformers
# from .utils import worker_process, worker_init, attack_process
import tqdm
import json
from joblib import Parallel, delayed
from joblib import Memory
import torch
import torch.nn as nn
import matplotlib.pyplot as plt
from sklearn.preprocessing import MinMaxScaler, StandardScaler
import reconstructor_net
import matplotlib.pyplot as plt

model_name = "Ramamurthi/distilbert-base-uncased-finetuned-yelp-reviews"
tokenizer = transformers.AutoTokenizer.from_pretrained(model_name)

torch.manual_seed(9001)
np.random.seed(9001)


def read_explanations(file_name="data/explanations/yelp_explanations_0-50466.out"):
    ## all explantions["1 star"] will contain a list off all explanations for reviews predicted to be 1 star.
    all_explanations = dict()

    # file_name="explanations.out"
    with open(file_name, 'r', encoding="utf-8") as f:
        for line in f:
            explanation = json.loads(line)
            if len(explanation["input_expl"][
                       "1 star"]) < max_tokens_per_review:  # It does not matter if we use 1 star, all explanations should have same tokens
                predicted_class = str(explanation["predicted_class"])
                if not predicted_class in all_explanations:  ##check if key exists
                    all_explanations[predicted_class] = list()
                all_explanations[predicted_class].append(explanation["input_expl"][predicted_class])

    generator1 = torch.Generator().manual_seed(9001)
    train_explanations = dict()
    test_explanations = dict()
    for stars in sorted(all_explanations.keys()):
        train_set, test_set = torch.utils.data.random_split(all_explanations[stars], [0.8, 0.2], generator=generator1)
        train_explanations[stars] = [data for data in train_set]
        test_explanations[stars] = [data for data in test_set]
    return train_explanations, test_explanations


max_tokens_per_review = 300


###Tensor format (max_tokens_per_review) (1d):
# For every token in the review the tensor contains the respective word saliency (float)
def explanation_list_to_simple_tensor(explanations_list):
    explanation_tensor = torch.zeros(1)
    for word in explanations_list:
        # simply create a list of the word saliencies in the order the words occur in the review
        explanation_tensor = torch.cat((explanation_tensor, torch.tensor(word[1]).unsqueeze(0)), dim=0)

    input_len = explanation_tensor.size(0)
    if max_tokens_per_review > input_len:
        padding_len = max_tokens_per_review - input_len
        padding_tensor = torch.zeros(padding_len)
        explanation_tensor = torch.cat((explanation_tensor, padding_tensor), dim=0)

    return explanation_tensor


##read explanations from file and return a dict of tensors (that are "stacked" simple tensors), 1 tensor for each class
def get_simple_tensors():
    all_explanations, _ = read_explanations()

    # print some stats about the data
    for stars in sorted(all_explanations.keys()):
        print("Number of inputs with predicted class " + str(stars) + ": " + str(len(all_explanations[stars])))

    all_simple_tensors = dict()
    for stars in all_explanations.keys():
        all_simple_tensors[stars] = torch.zeros(1, max_tokens_per_review)
        for explanation in all_explanations[stars]:
            explanation_tensor = explanation_list_to_simple_tensor(explanation)
            all_simple_tensors[stars] = torch.cat((all_simple_tensors[stars], explanation_tensor.unsqueeze(0)), dim=0)
        print("non-zero entires: " + str(torch.nonzero(all_simple_tensors[stars]).size(0)))
    return all_simple_tensors


###Tensor format (max_tokens_per_review * 3+1) (1d):
def explanation_list_to_complexv2_tensor(explanations_list):
    explanation_tensor = torch.zeros(1)
    for word in explanations_list["input_expl"]:
        # print(word[0])
        input_ids = tokenizer(word[0])["input_ids"]
        word_importance = word[1]
        word_tensor = torch.zeros(2, 3)
        word_tensor = torch.tensor([input_ids[0], input_ids[1], input_ids[2], word_importance])
        # print(word_tensor)
        explanation_tensor = torch.cat((explanation_tensor, word_tensor), dim=0)

    # Padding
    # print("explanation_tensor shape before padding")
    # print(explanation_tensor.shape)

    input_len = explanation_tensor.shape[0]  # number of words/tokens in the review
    if (max_tokens_per_review * 4) < input_len:
        print("Error: Review conains more words than expected!")
    if (max_tokens_per_review * 4) > input_len:
        padding_len = (max_tokens_per_review * 4) - input_len

        # Create a padding tensor consisting of zeros
        # For our tokenizer, the pad token [PAD] has the token id 0:
        # print(tokenizer.pad_token_id)
        # The word saliency for the "padding words" should also be 0
        padding_tensor = torch.zeros(padding_len)
        # Append the padding to the explanation tensor
        explanation_tensor = torch.cat((explanation_tensor, padding_tensor), dim=0)

    # print("non-zero entires: " + str(torch.nonzero(explanation_tensor).size(0)))

    return explanation_tensor


def train_with_simple_tensors():
    input_size = max_tokens_per_review  # Still need to replace with actual size of our data
    hidden_size = int(
        input_size * 0.08)  # Choose the appropriate size for the hidden layer. The paper used 8% of input layer size.
    output_size = max_tokens_per_review  # Still need to replace with actual size of our data

    explanation_tensors_dict = get_simple_tensors()

    for stars in sorted(explanation_tensors_dict.keys()):
        print(stars)
        explanations_tensor = explanation_tensors_dict[stars]
        scaler = MinMaxScaler((0, 1))
        scaled_explanations_tensor_numpy = scaler.fit_transform(explanations_tensor.numpy())
        scaled_explanations_tensor = torch.tensor(scaled_explanations_tensor_numpy, dtype=torch.float32)
        explanations_dataset = reconstructor_net.ExplanationDataset(scaled_explanations_tensor,
                                                                    len(explanation_tensors_dict[stars]),
                                                                    max_tokens_per_review)
        dataloader = reconstructor_net.DataLoader(explanations_dataset, batch_size=64, shuffle=True)

        # Create and train the model for positive inputs
        model = reconstructor_net.EncoderDecoder(input_size, hidden_size, output_size)
        fig, ax = model.train_network(dataloader)
        # save loss curve
        fig.savefig("loss_curve_" + str(stars) + "_model_simple_tensors.png")
        torch.save(model, str(stars) + "_model_simple_tensors_new.pts")


train_with_simple_tensors()
# train_with_comlexv2_tensors()




import pandas as pd
import time  ##for performance meassureing
import numpy as np
import transformers
# from .utils import worker_process, worker_init, attack_process
import tqdm
import json
from joblib import Parallel, delayed
from joblib import Memory
import torch
import reconstructor_net

model_name = "distilbert-base-uncased-finetuned-sst-2-english"
tokenizer = transformers.AutoTokenizer.from_pretrained(model_name)


def read_explanations(file_name="data/explanations/imdb_explanations_24876.out"):
    positive_explanations = list()
    negative_explanations = list()

    with open(file_name, 'r', encoding="utf-8") as f:
        for line in f:
            explanation = json.loads(line)
            if len(explanation["input_expl"]) < max_tokens_per_review:
                if explanation["predicted_class"] == "POSITIVE":
                    positive_explanations.append(explanation)
                elif explanation["predicted_class"] == "NEGATIVE":
                    negative_explanations.append(explanation)
    return positive_explanations, negative_explanations


max_tokens_per_review = 300


###Tensor format (max_tokens_per_review) (1d):
# For every token in the review the tensor contains the respective word saliency (float)
def explanation_list_to_simple_tensor(explanations_list):
    explanation_tensor = torch.zeros(1)
    for word in explanations_list["input_expl"]:
        # simply create a list of the word saliencies in the order the words occur in the review
        explanation_tensor = torch.cat((explanation_tensor, torch.tensor(word[1]).unsqueeze(0)), dim=0)

    input_len = explanation_tensor.size(0)
    if max_tokens_per_review > input_len:
        padding_len = max_tokens_per_review - input_len
        padding_tensor = torch.zeros(padding_len)
        explanation_tensor = torch.cat((explanation_tensor, padding_tensor), dim=0)

    return explanation_tensor


###TODO: Is there a smarter way to build the tensors/smaller size?
# Note: The returned tensor is 1d because it is reshaped at the end
###Tensor format (max_tokens_per_review,2,3) (3d):
## 1.dim: Every review can have up to max_tokens_per_review words
## 2.dim: For every word we store the word(ids) and the importance of the word
## 3.dim: The tokenizer transforms each word into a list of 3 integers
def explanation_list_to_complex_tensor(explanations_list):
    explanation_tensor = torch.zeros(1, 2, 3)
    for word in explanations_list["input_expl"]:
        # print(word[0])
        input_ids = tokenizer(word[0])["input_ids"]
        word_importance = word[1]
        word_tensor = torch.zeros(2, 3)
        word_tensor = torch.tensor([[input_ids[0], input_ids[1], input_ids[2]], [word_importance] * 3])
        # print(word_tensor)
        explanation_tensor = torch.cat((explanation_tensor, word_tensor.unsqueeze(0)), dim=0)

    # Padding
    # print("explanation_tensor shape before padding")
    # print(explanation_tensor.shape)

    input_len = explanation_tensor.shape[0]  # number of words/tokens in the review
    if max_tokens_per_review < input_len:
        print("Error: Review conains more words than expected!")
    if max_tokens_per_review > input_len:
        padding_len = max_tokens_per_review - input_len

        # Create a padding tensor consisting of zeros
        # For our tokenizer, the pad token [PAD] has the token id 0:
        # print(tokenizer.pad_token_id)
        # The word saliency for the "padding words" should also be 0
        padding_tensor = torch.zeros(padding_len, 2, 3)
        # Append the padding to the explanation tensor
        explanation_tensor = torch.cat((explanation_tensor, padding_tensor), dim=0)

    # print("explanation_tensor shape after padding")
    # print(explanation_tensor.shape)

    # print("explanation_tensor inside of function")
    # print(explanation_tensor)
    # print("non-zero entires: " + str(torch.nonzero(explanation_tensor).size(0)))

    # Reshape to a 1d tensor
    explanation_tensor_reshaped = explanation_tensor.reshape(max_tokens_per_review * 2 * 3)  # =1800
    return explanation_tensor_reshaped


###Tensor format (max_tokens_per_review * 3+1) (1d):
def explanation_list_to_complexv2_tensor(explanations_list):
    explanation_tensor = torch.zeros(1)
    for word in explanations_list["input_expl"]:
        # print(word[0])
        input_ids = tokenizer(word[0])["input_ids"]
        word_importance = word[1]
        word_tensor = torch.zeros(2, 3)
        word_tensor = torch.tensor([input_ids[0], input_ids[1], input_ids[2], word_importance])
        # print(word_tensor)
        explanation_tensor = torch.cat((explanation_tensor, word_tensor), dim=0)

    # Padding
    # print("explanation_tensor shape before padding")
    # print(explanation_tensor.shape)

    input_len = explanation_tensor.shape[0]  # number of words/tokens in the review
    if (max_tokens_per_review * 4) < input_len:
        print("Error: Review conains more words than expected!")
    if (max_tokens_per_review * 4) > input_len:
        padding_len = (max_tokens_per_review * 4) - input_len

        # Create a padding tensor consisting of zeros
        # For our tokenizer, the pad token [PAD] has the token id 0:
        # print(tokenizer.pad_token_id)
        # The word saliency for the "padding words" should also be 0
        padding_tensor = torch.zeros(padding_len)
        # Append the padding to the explanation tensor
        explanation_tensor = torch.cat((explanation_tensor, padding_tensor), dim=0)

    # print("non-zero entires: " + str(torch.nonzero(explanation_tensor).size(0)))

    return explanation_tensor


def create_vocab():
    positive_explanations, negative_explanations = read_explanations()

    vocab = set()
    for explanation in positive_explanations:
        for word in explanation["input_expl"]:
            vocab.add(word[0])
    for explanation in negative_explanations:
        for word in explanation["input_expl"]:
            vocab.add(word[0])
    word_to_ix = {word: i for i, word in enumerate(vocab)}
    return vocab, word_to_ix


def explanation_list_to_bow_tensor(explanations_list, vocab, word_to_ix):
    bow_tensor = torch.zeros(len(vocab))
    for word in explanations_list["input_expl"]:
        if word[0] in vocab:
            bow_tensor[word_to_ix[word[0]]] = word[1]
        # else:
        # ignore the word
    return bow_tensor


def train_with_simple_tensors():
    input_size = max_tokens_per_review  # Still need to replace with actual size of our data
    hidden_size = int(
        input_size * 0.08)  # Choose the appropriate size for the hidden layer. The paper used 8% of input layer size.
    output_size = max_tokens_per_review  # Still need to replace with actual size of our data

    positive_explanations, negative_explanations = read_explanations()
    print("Number of inputs with predicted class POSITIVE: " + str(len(positive_explanations)))
    print("Number of inputs with predicted class NEGATIVE: " + str(len(negative_explanations)))
    # 6k enough for training given paper
    positive_explanations = positive_explanations[:6000]
    negative_explanations = negative_explanations[:6000]

    # pos_data = torch.empty((len(positive_explanations),max_tokens_per_review * 2 * 3))
    pos_data = torch.zeros(1, max_tokens_per_review)

    for explanation in positive_explanations:
        explanation_tensor = explanation_list_to_simple_tensor(explanation)
        # print("non-zero entires: " + str(torch.nonzero(explanation_tensor).size(0)))
        pos_data = torch.cat((pos_data, explanation_tensor.unsqueeze(0)), dim=0)

    # neg_data = torch.empty((len(negative_explanations),max_tokens_per_review * 2 * 3))
    neg_data = torch.zeros(1, max_tokens_per_review)

    for explanation in negative_explanations:
        explanation_tensor = explanation_list_to_simple_tensor(explanation)
        # print("non-zero entires: " + str(torch.nonzero(explanation_tensor).size(0)))
        neg_data = torch.cat((neg_data, explanation_tensor.unsqueeze(0)), dim=0)

    pos_dataset = reconstructor_net.ExplanationDataset(len(positive_explanations), max_tokens_per_review)
    pos_dataset.data = pos_data

    neg_dataset = reconstructor_net.ExplanationDataset(len(negative_explanations), max_tokens_per_review)
    neg_dataset.data = neg_data

    pos_dataloader = reconstructor_net.DataLoader(pos_dataset, batch_size=1, shuffle=True)
    neg_dataloader = reconstructor_net.DataLoader(neg_dataset, batch_size=1, shuffle=True)

    # Create and train the model for positive inputs
    pos_model = reconstructor_net.EncoderDecoder(input_size, hidden_size, output_size)
    pos_model.train_network(pos_dataloader)
    torch.save(pos_model, "pos_model_test.pts")


    # Create and train the model for negative inputs
    neg_model = reconstructor_net.EncoderDecoder(input_size, hidden_size, output_size)
    neg_model.train_network(neg_dataloader)
    torch.save(neg_model, "neg_model_test.pts")




def train_with_comlex_tensors():
    input_size = max_tokens_per_review * 2 * 3  # Still need to replace with actual size of our data
    hidden_size = int(
        input_size * 0.08)  # Choose the appropriate size for the hidden layer. The paper used 8% of input layer size.
    output_size = max_tokens_per_review * 2 * 3  # Still need to replace with actual size of our data

    positive_explanations, negative_explanations = read_explanations()
    print("Number of inputs with predicted class POSITIVE: " + str(len(positive_explanations)))
    print("Number of inputs with predicted class NEGATIVE: " + str(len(negative_explanations)))

    # pos_data = torch.empty((len(positive_explanations),max_tokens_per_review * 2 * 3))
    pos_data = torch.zeros(1, max_tokens_per_review * 2 * 3)

    for explanation in positive_explanations:
        explanation_tensor = explanation_list_to_complex_tensor(explanation)
        # print("non-zero entires: " + str(torch.nonzero(explanation_tensor).size(0)))
        pos_data = torch.cat((pos_data, explanation_tensor.unsqueeze(0)), dim=0)

    # neg_data = torch.empty((len(negative_explanations),max_tokens_per_review * 2 * 3))
    neg_data = torch.zeros(1, max_tokens_per_review * 2 * 3)

    for explanation in negative_explanations:
        explanation_tensor = explanation_list_to_complex_tensor(explanation)
        # print("non-zero entires: " + str(torch.nonzero(explanation_tensor).size(0)))
        neg_data = torch.cat((neg_data, explanation_tensor.unsqueeze(0)), dim=0)

    pos_dataset = reconstructor_net.ExplanationDataset(len(positive_explanations), max_tokens_per_review * 2 * 3)
    pos_dataset.data = pos_data

    neg_dataset = reconstructor_net.ExplanationDataset(len(negative_explanations), max_tokens_per_review * 2 * 3)
    neg_dataset.data = neg_data

    pos_dataloader = reconstructor_net.DataLoader(pos_dataset, batch_size=1, shuffle=True)
    neg_dataloader = reconstructor_net.DataLoader(neg_dataset, batch_size=1, shuffle=True)

    # Create and train the model for positive inputs
    pos_model = reconstructor_net.EncoderDecoder(input_size, hidden_size, output_size)
    pos_model.train(pos_dataloader)
    torch.save(pos_model, "pos_model_complex_tensors.pts")

    # Create and train the model for negative inputs
    neg_model = reconstructor_net.EncoderDecoder(input_size, hidden_size, output_size)
    neg_model.train(neg_dataloader)
    torch.save(neg_model, "neg_model_complex_tensors.pts")


def train_with_comlexv2_tensors():
    input_size = max_tokens_per_review * 4  # Still need to replace with actual size of our data
    hidden_size = int(
        input_size * 0.08)  # Choose the appropriate size for the hidden layer. The paper used 8% of input layer size.
    output_size = max_tokens_per_review * 4  # Still need to replace with actual size of our data

    positive_explanations, negative_explanations = read_explanations()
    print("Number of inputs with predicted class POSITIVE: " + str(len(positive_explanations)))
    print("Number of inputs with predicted class NEGATIVE: " + str(len(negative_explanations)))

    pos_data = torch.zeros(1, max_tokens_per_review * 4)

    for explanation in positive_explanations:
        explanation_tensor = explanation_list_to_complexv2_tensor(explanation)
        # print("non-zero entires: " + str(torch.nonzero(explanation_tensor).size(0)))
        pos_data = torch.cat((pos_data, explanation_tensor.unsqueeze(0)), dim=0)

    neg_data = torch.zeros(1, max_tokens_per_review * 4)

    for explanation in negative_explanations:
        explanation_tensor = explanation_list_to_complexv2_tensor(explanation)
        # print("non-zero entires: " + str(torch.nonzero(explanation_tensor).size(0)))
        neg_data = torch.cat((neg_data, explanation_tensor.unsqueeze(0)), dim=0)

    pos_dataset = reconstructor_net.ExplanationDataset(len(positive_explanations), max_tokens_per_review * 2 * 3)
    pos_dataset.data = pos_data

    neg_dataset = reconstructor_net.ExplanationDataset(len(negative_explanations), max_tokens_per_review * 2 * 3)
    neg_dataset.data = neg_data

    pos_dataloader = reconstructor_net.DataLoader(pos_dataset, batch_size=1, shuffle=True)
    neg_dataloader = reconstructor_net.DataLoader(neg_dataset, batch_size=1, shuffle=True)

    # Create and train the model for positive inputs
    pos_model = reconstructor_net.EncoderDecoder(input_size, hidden_size, output_size)
    pos_model.train(pos_dataloader)
    torch.save(pos_model, "pos_model_complexv2_tensors.pts")

    # Create and train the model for negative inputs
    neg_model = reconstructor_net.EncoderDecoder(input_size, hidden_size, output_size)
    neg_model.train(neg_dataloader)
    torch.save(neg_model, "neg_model_complexv2_tensors.pts")


def train_with_bow_tensors():
    vocab, word_to_ix = create_vocab()
    vocab_size = len(vocab)
    input_size = vocab_size  # Still need to replace with actual size of our data
    hidden_size = int(
        input_size * 0.08)  # Choose the appropriate size for the hidden layer. The paper used 8% of input layer size.
    output_size = vocab_size * 2 * 3  # Still need to replace with actual size of our data

    positive_explanations, negative_explanations = read_explanations()
    print("Number of inputs with predicted class POSITIVE: " + str(len(positive_explanations)))
    print("Number of inputs with predicted class NEGATIVE: " + str(len(negative_explanations)))
    print("Number of words in the vocab: " + str(vocab_size))

    # pos_data = torch.empty((len(positive_explanations),max_tokens_per_review * 2 * 3))
    pos_data = torch.zeros(1, vocab_size)

    for explanation in positive_explanations:
        explanation_tensor = explanation_list_to_bow_tensor(explanation, vocab, word_to_ix)
        # print("non-zero entires: " + str(torch.nonzero(explanation_tensor).size(0)))
        pos_data = torch.cat((pos_data, explanation_tensor.unsqueeze(0)), dim=0)

    # neg_data = torch.empty((len(negative_explanations),max_tokens_per_review * 2 * 3))
    neg_data = torch.zeros(1, vocab_size)

    for explanation in negative_explanations:
        explanation_tensor = explanation_list_to_bow_tensor(explanation, vocab, word_to_ix)
        # print("non-zero entires: " + str(torch.nonzero(explanation_tensor).size(0)))
        neg_data = torch.cat((neg_data, explanation_tensor.unsqueeze(0)), dim=0)

    pos_dataset = reconstructor_net.ExplanationDataset(len(positive_explanations), max_tokens_per_review * 2 * 3)
    pos_dataset.data = pos_data

    neg_dataset = reconstructor_net.ExplanationDataset(len(negative_explanations), max_tokens_per_review * 2 * 3)
    neg_dataset.data = neg_data

    pos_dataloader = reconstructor_net.DataLoader(pos_dataset, batch_size=1, shuffle=True)
    neg_dataloader = reconstructor_net.DataLoader(neg_dataset, batch_size=1, shuffle=True)

    # Create and train the model for positive inputs
    pos_model = reconstructor_net.EncoderDecoder(input_size, hidden_size, output_size)
    pos_model.train(pos_dataloader)
    torch.save(pos_model, "pos_model_bow_tensors.pts")

    # Create and train the model for negative inputs
    neg_model = reconstructor_net.EncoderDecoder(input_size, hidden_size, output_size)
    neg_model.train(neg_dataloader)
    torch.save(neg_model, "neg_model_bow_tensors.pts")


train_with_simple_tensors()
# train_with_comlex_tensors()
#train_with_comlexv2_tensors()
# train_with_bow_tensors()